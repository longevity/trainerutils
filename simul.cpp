#include "simul.h"
#include "single.h"
#include "SimpleIni.h"

#include <QDir>
#include <QFile>
#include <QIODevice>
#include <QTextStream>
#include <QDateTime>
#include <QSpinBox>
#include <QCoreApplication>

void Simul::overwritem_simulini()
{
    QString platfName = QObject::sender()->objectName();

    int valOfSpinBox = this->get_value();

    QDir dir = QDir::current();
         dir.cdUp();
		 QString path = QDir::toNativeSeparators(dir.path() + this->getShipName() + "/" + Data::getInst().ToStrIni(Data::getInst().ini::SIMUL));

    QByteArray ba = path.toLatin1();

    const char* cs_path = ba.data();

    CSimpleIniA s;
                s.SetUnicode();
                s.SetSpaces();

    SI_Error rc = s.LoadFile(cs_path);
          if(rc < 0 || s.IsEmpty()) return; // strerror_s("Oh dear, something went wrong with read() ?! \n", 47, errno);
    
    bool b_2_1 = s.GetSection("Starting Conditions.Platform2.1");
    bool b_2_2 = s.GetSection("Starting Conditions.Platform2.2");

    bool b_3_1 = s.GetSection("Starting Conditions.Platform3.1");
    bool b_3_2 = s.GetSection("Starting Conditions.Platform3.2");

    if(valOfSpinBox == 1){
        Simul::m_map = Simul::get_map(1);
        if(platfName == "Platform2"){
            if(b_2_1) {
                const char *section = Simul::getSection(platfName, 1);
                const char *crt  = Simul::getCrt(platfName);
                s.SetValue(section, "Chart.Name", crt);
                const char *wal = Simul::getWal(platfName);
                s.SetValue(section, "Chart.NameWal", wal);
            } else {
                for(auto it = m_map.begin(); it != m_map.end(); ++it)
                    s.SetValue("Starting Conditions.Platform2.1", it->first, it->second);
            }
        } else
        if(platfName == "Platform3"){
            if(b_3_1) {
                const char *section = Simul::getSection(platfName, 1);
                const char *crt  = Simul::getCrt(platfName);
                s.SetValue(section, "Chart.Name", crt);
                const char *wal = Simul::getWal(platfName);
                s.SetValue(section, "Chart.NameWal", wal);
            } else {
                for(auto it = m_map.begin(); it != m_map.end(); ++it)
                    s.SetValue("Starting Conditions.Platform3.1", it->first, it->second);
            }
        }
    } else
    if(valOfSpinBox == 2){
        if(platfName == "Platform2"){
            if(b_2_1){
                const char *section = Simul::getSection(platfName, 1);
                const char *crt  = Simul::getCrt(platfName);
                s.SetValue(section, "Chart.Name", crt);
                const char *wal = Simul::getWal(platfName);
                s.SetValue(section, "Chart.NameWal", wal);
            } else {
                Simul::m_map = Simul::get_map(1);
                for(auto it = m_map.begin(); it != m_map.end(); ++it)
                    s.SetValue("Starting Conditions.Platform2.1", it->first, it->second);
            }

            if(b_2_2){
                const char *section = Simul::getSection(platfName, 2);
                const char *crt  = Simul::getCrt(platfName);
                s.SetValue(section, "Chart.Name", crt);
                const char *wal = Simul::getWal(platfName);
                s.SetValue(section, "Chart.NameWal", wal);
            } else {
                Simul::m_map = Simul::get_map(2);
                for(auto it = m_map.begin(); it != m_map.end(); ++it)
                    s.SetValue("Starting Conditions.Platform2.2", it->first, it->second);
            }
        } else if(platfName == "Platform3"){
            if(b_3_1){
                const char *section = Simul::getSection(platfName, 1);
                const char *crt = Simul::getCrt(platfName);
                s.SetValue(section, "Chart.Name", crt);
                const char *wal = Simul::getWal(platfName);
                s.SetValue(section, "Chart.NameWal", wal);
            } else {
                Simul::m_map = Simul::get_map(1);
                Simul::m_map["Chart.Name"] = "Platform3.crt";
                Simul::m_map["Chart.NameWal"] = "Platform3.wal";

                for(auto it = m_map.begin(); it != m_map.end(); ++it)
                    s.SetValue("Starting Conditions.Platform3.1", it->first, it->second);
            }

            if(b_3_2){
                const char *section = Simul::getSection(platfName, 2);
                const char *crt = Simul::getCrt(platfName);
                s.SetValue(section, "Chart.Name", crt);
                const char *wal = Simul::getWal(platfName);
                s.SetValue(section, "Chart.NameWal", wal);
            } else {
                Simul::m_map = Simul::get_map(2);
                Simul::m_map["Chart.Name"] = "Platform3.crt";
                Simul::m_map["Chart.NameWal"] = "Platform3.wal";

                for(auto it = m_map.begin(); it != m_map.end(); ++it)
                    s.SetValue("Starting Conditions.Platform3.2", it->first, it->second);
            }
        } else {
            return;
        }
    } else {
        return;
    }

    const char* cs_platform = getScenario(platfName, valOfSpinBox);
    s.SetValue("Simulator", "Scenario", cs_platform);

    if(valOfSpinBox == 1){
        cs_platform = getKnownScenario(platfName, 1);
        s.SetValue("Simulator", "KnownScenario", cs_platform);
    } else
    if(valOfSpinBox == 2){
        cs_platform = getKnownScenario(platfName, 2);
        s.SetValue("Simulator", "KnownScenario", cs_platform);
    }

    s.SaveFile(cs_path);
}

void Simul::setShipName(){ this->m_shipName = QObject::sender()->objectName(); }

void Simul::spinboxValueChanged(int value){ this->m_value = value; }

const char *
     Simul::getCrt(const QString & platfName) const
{
    QString crt = platfName + ".crt";
    QByteArray ba = crt.toLatin1();
    const char* cs_crt = ba.data();

    return cs_crt;
}

const char *
     Simul::getWal(const QString & platfName) const
{
    QString wal = platfName + ".wal";
    QByteArray ba = wal.toLatin1();
    const char* cs_wal = ba.data();

    return cs_wal;
}

const char *
     Simul::getSection(const QString & platfName, int num) const
{
    QString section = "Starting Conditions." + platfName + "." + QString::number(num);
    QByteArray ba = section.toLatin1();
    const char* cs_section = ba.data();

    return cs_section;
}

const char *
     Simul::getScenario(QString platfName, int valOfSpinBox)
{
    QString platform = platfName + "." + QString::number(valOfSpinBox);
    QByteArray ba = platform.toLatin1();
    const char* cs_platform = ba.data();

    return cs_platform;
}

const char *
     Simul::getKnownScenario(QString platfName, int num)
{
    QString platform;

    switch(num){
    case 2 :
        platform = platfName + ".1, " + platfName + ".2";
        break;
    case 1 :
        platform = platfName + ".1";
        break;
    default :
        break;
    }

    QByteArray ba = platform.toLatin1();
    const char* cs_platform = ba.data();

    return cs_platform;
}
