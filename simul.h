#ifndef SIMUL_H
#define SIMUL_H

#ifndef DATA_H
#define DATA_H
#endif DATA_H

#include <QObject>
#include <QString>

#include "SimpleIni.h"
#include "Data.h"

class Simul : public QObject
{
    Q_OBJECT
public:
	Simul::Simul() : m_value(1), m_shipName(Data::getInst().getInitialShip()) {}
    int get_value(){ return m_value; }
    QString getShipName() { return m_shipName; }

private:
    int m_value;
    QString m_shipName;
    std::map<const char *, const char *> get_map(int num)
    {
        bool one = (num == 1);
        std::map<const char *,
			const char *> map = Data::getInst().getInitSettingsSimulIni(one);          
        return map;
    }
    std::map<const char*, const char*> m_map;
	
    const char* getCrt(const QString & platfName) const;
    const char* getWal(const QString & platfName) const;
    const char* getSection(const QString & platfName, int num) const;
    const char* getScenario(QString platfName, int num);
    const char* getKnownScenario(QString platfName, int num);

public slots:
    void spinboxValueChanged(int value);
    void overwritem_simulini();
    void setShipName();
};

#endif




