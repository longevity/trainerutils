#pragma once

#include <QString>
#include <string>
#include <map>
#include <iostream>
#include <new>

class Data
{
private:
	Data() = default;
	~Data() = default;

	Data(const Data&) = delete;
	Data& operator=(const Data&) = delete;

	void * operator new(std::size_t) = delete;
	void * operator new[](std::size_t) = delete;

	void operator delete(void*) = delete;
	void operator delete[](void*) = delete;

	template <typename Key, typename T>
	class m_OrderedMap
	{
	public:
		// ... all your functions here, straigtforward enough to use indexOf()...
	private:
		QList<Key> m_keys;
		QList<T> m_values;
	};

public:
	static Data& getInst(){
		static Data data;
		return data;
	}

	QString getInitialShip(){ return "_Nav_Arctech_NB510"; }

	std::map<std::string,
		std::string> getShips(){
			std::map<std::string,
				std::string> m_ships;
			m_ships["Arctech NB510"] = "_Nav_Arctech_NB510";
			m_ships["Blue Antares"] = "_Nav_BlueAntares";
			m_ships["Blue Aries"] = "_Nav_BlueAries";
			m_ships["Nordic Stavanger"] = "_Nav_NordicStavanger_2";
			m_ships["OSV1"] = "_Nav_OSV1_2";
			m_ships["OSV2"] = "_Nav_OSV2_2";
			m_ships["OSV3"] = "_Nav_OSV3_2";
			m_ships["OSV11"] = "_Nav_OSV11_2";
			m_ships["Platform"] = "_Nav_Platform_2";
			m_ships["Red Husky"] = "_Nav_RedHusky_2";
			m_ships["Vickie Tide"] = "_Nav_VickieTide_2";
			return m_ships;
		}

	std::map<const char *,
		const char *> getInitSettingsSimulIni(bool one){
			std::map<const char *,
				const char *> initSettingsSimulIni;
			initSettingsSimulIni["Vx (m/s)"] = "0";
			initSettingsSimulIni["Xg (m)"] = (one) ? "6600" : "12200";
			initSettingsSimulIni["Yg (m)"] = (one) ? "4600" : "12600";
			initSettingsSimulIni["Course (deg)"] = "0";
			initSettingsSimulIni["BaseLon"] = "0";
			initSettingsSimulIni["BaseLat"] = "0";
			initSettingsSimulIni["Chart.Path"] = ".\\";
			initSettingsSimulIni["Chart.Name"] = "Platform2.crt";
			initSettingsSimulIni["Chart.NameWal"] = "Platform2.wal";
			return initSettingsSimulIni;
		}

	enum executable { STARTER, LAUNCHER };
	enum icons { SHIP_ICO, SHIP_ICON };
	enum ini { SYS, LOG, SYSTEM, SIMUL };
	enum images { NAVIS, LOGO };
	enum widgets { EXECUTE, ABOUT, TRAINER_UTIL, VISUAL, PLATFORM, SETTINGS_SIMUL, SETTINGS_VISUAL, QUIT };
	enum messages { ABOUT_PROGRAMM, APP_INIT, COMPANY, LOADING_MODULES };
	enum path { IMAGES, TWO_POINTS };
	enum params { P_RGB, P_DATE, WIN_CODEC, UTF_CODEC };
	enum time { SPLASH_TIME = 5000 };
	enum section { PORTS, SERVER, VISSERVER, SECTION_VISUAL, PORTS_SERVER, PORTS_VISSERVER, SERVER_IP_DPA, SERVER_IP_DPB, IP_DPA, IP_DPB };
	enum network { ALL_IP, IP_ALL, IP_VIS, IP_VIS_ALL };

	inline QString ToStrImages(images e_images){
		switch (e_images){ // enum images 
		case NAVIS: return "Navis.png";
		case LOGO: return "logo.png";
		default: return "";
		}
	}

	inline QString ToStrIcons(icons e_icons){
		switch (e_icons){ // enum icons
		case SHIP_ICO: return "ship.ico";
		case SHIP_ICON: return "ship.icon";
		default: return "";
		}
	}

	inline QString ToStrIni(ini e_ini){
		switch (e_ini){ // enum ini
		case SYS: return "sys.ini";
		case LOG: return "log.ini";
		case SYSTEM: return "system.ini";
		case SIMUL: return "simul.ini";
		default: return "";
		}
	}

	inline QString ToStrWidgets(widgets e_widgets){
		switch (e_widgets){ // enum widgets
		case ABOUT: return "About";
		case EXECUTE: return "Execute";
		case TRAINER_UTIL: return "TrainerUtil";
		case VISUAL: return "Visual";
		case PLATFORM: return "Platform";
		case SETTINGS_SIMUL: return "Settings [Simul.ini]";
		case SETTINGS_VISUAL: return "Settings [Visual.ini]";
		case QUIT: return "Quit";
		default: return "";
		}
	}

	inline QString ToStrMessages(messages e_messages){
		switch (e_messages){ // enum messages
		case ABOUT_PROGRAMM: return "About program...";
		case APP_INIT: return "Application initialization...";
		case COMPANY: return "Company:  Navis Engineering\nDeveloper: Vadim Kulikov\n� " \
			"2016 Navis Company. All rights reserved.";
		case LOADING_MODULES: return "Loading modules...";
		default: return "";
		}
	}

	inline QString ToStrExecutable(executable e_executable){
		switch (e_executable){ // enum executable
		case STARTER: return "Starter.exe.cmd";
		case LAUNCHER: return "Launcher.cmd";
		default: return "";
		}
	}

	inline QString ToStrPath(path e_path){
		switch (e_path){ // enum path 
		case IMAGES: return ":/images/";
		case TWO_POINTS: return "../";
		default: return "";
		}
	}

	inline QString ToStrNetwork(network e_network){
		switch (e_network){ // enum network
		case ALL_IP: return "all ip addresses";
		case IP_ALL: return "ipAll";
		case IP_VIS: return "ipVis";
		case IP_VIS_ALL: return "ipVisAll";
		default: return "";
		}
	}

	inline QString ToStrSection(section e_section){
		switch (e_section){ // enum section
		case PORTS: return "Ports";
		case SERVER: return "Server";
		case VISSERVER: return "VisServer";
		case SECTION_VISUAL: return "Visual";
		case PORTS_SERVER: return "Ports/Server";
		case PORTS_VISSERVER: return "Ports/VisServer";
		case SERVER_IP_DPA: return "Server/ipDPA";
		case SERVER_IP_DPB: return "Server/ipDPB";
		case IP_DPA: return "ipDPA";
		case IP_DPB: return "ipDPB";
		default: return "";
		}
	}

	inline QString ToStrParams(params e_params){
		switch (e_params){ // enum params
		case P_RGB: return "QTextEdit { background-color: rgb(240, 240, 240) }";
		case P_DATE: return "dd.MM.yy hh:mm ";
		case WIN_CODEC: return "Windows-1251";
		case UTF_CODEC: return "UTF-8";
		default: return "";
		}
	}
};