#include "single.h"
#include "simul.h"
#include "mytcpsocket.h"

#include <QApplication>
#include <QMessageBox>
#include <QDesktopWidget>
#include <QSize>
#include <QIcon>
#include <QDir>
#include <QSettings>
#include <QDateTime>
#include <QProcess>
#include <QTextCodec>

#include "SimpleIni.h"

auto & data = Data::getInst(); // anti-pattern singleton

void Single::showAboutProgram()
{
    QMessageBox* mb = new QMessageBox();

    mb->setFixedSize(QSize(200, 100));

	mb->setWindowIcon(QIcon(data.ToStrIcons(data.icons::SHIP_ICON)));

    QDesktopWidget *desk = QApplication::desktop();

    int scrWidth  = desk->width();
    int scrHeight = desk->height();

    int x = (scrWidth  - 200) / 2;
    int y = (scrHeight - 100) / 2;

    mb->setGeometry(x, y, 200, 100);

	mb->setWindowIcon(QIcon(data.ToStrIcons(data.icons::SHIP_ICO)));

	mb->setWindowTitle(data.ToStrMessages(data.messages::ABOUT_PROGRAMM));
	mb->setText(data.ToStrMessages(data.messages::COMPANY));
    mb->exec();

    delete mb;
}

void Single::overwriteSystemINI()
{
    QDir dir = QDir::current();
         dir.cdUp();
    QString path = dir.path() +"/" + data.ToStrIni(data.ini::SYSTEM);

    QString shipName = this->getShipName();

    CSimpleIniA system;
                system.SetUnicode();

                QByteArray ba = path.toLatin1();

                   const char* cs_path = ba.data();
                system.LoadFile(cs_path);
                                           const char* cs_shipName = shipName.toStdString().c_str();
                system.SetValue("System Path", "ship", cs_shipName);
                system.SaveFile(cs_path);
}

void Single::startVisualPrograms(void)
{
	QSettings sys(data.ToStrIni(data.ini::SYS), QSettings::IniFormat);
              sys.sync();

    QVector<QString> IpList;

	sys.beginGroup(data.ToStrSection(data.section::SECTION_VISUAL));

    foreach(QString key, sys.allKeys())
    {
		bool isKeyIpVisExists = key.contains(data.ToStrNetwork(data.network::IP_VIS), Qt::CaseInsensitive);
		bool isKeym_ipallExists = key.contains(data.ToStrNetwork(data.network::IP_ALL), Qt::CaseInsensitive);

        if(isKeyIpVisExists || isKeym_ipallExists){
            QString ip = sys.value(key, "").toString();
            IpList.append(ip);
        }
    }

    sys.endGroup();

	QString portVisServer = sys.value(data.ToStrSection(data.section::PORTS_VISSERVER)).toString();

    QString visName = QObject::sender()->objectName();

    qint64 port = portVisServer.toInt();

    MyTcpSocket socket;

	if (this->getIpAddress() == data.ToStrNetwork(data.network::ALL_IP)){
        foreach(QString val, IpList) {
			if (val == data.ToStrNetwork(data.network::ALL_IP)) continue;
            socket.doConnect(val, visName, port);
        }
    } else {
        QString ip = this->getIpAddress();
        socket.doConnect(ip, visName, port);
    }
    QVector<QString>().swap(IpList);
}

void Single::startAllProgramsOfSystem()
{
    QString shipName = this->getShipName();

	QSettings sys(data.ToStrIni(data.ini::SYS), QSettings::IniFormat);

	QString portServer = sys.value(data.ToStrSection(data.section::PORTS_SERVER)).toString();
	QString ipDPA = sys.value(data.ToStrSection(data.section::SERVER_IP_DPA)).toString();
	QString ipDPB = sys.value(data.ToStrSection(data.section::SERVER_IP_DPB)).toString();

    qint64 port = portServer.toInt();
    MyTcpSocket sipDPA;
                sipDPA.doConnect(ipDPA, shipName, port);

    MyTcpSocket sipDPB;
                sipDPB.doConnect(ipDPB, shipName, port);

    QProcess *launcher = new QProcess(this);
    QProcess *starter = new QProcess(this);

	launcher->startDetached(data.ToStrExecutable(data.executable::LAUNCHER));
    delete launcher;

	starter->startDetached(data.ToStrExecutable(data.executable::STARTER));
    delete starter;
}

void Single::setShipName(){ this->m_shipName = QObject::sender()->objectName(); }

void Single::comboboxValueChanged(QString value){ this->m_ipAddress = value; }

