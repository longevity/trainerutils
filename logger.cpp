#include "logger.h"
#include "SimpleIni.h"
#include "simul.h"
#include "Data.h"

#include <QSettings>
#include <QFile>
#include <QDir>

Logger::Logger(QWidget &parent, QString fileName, QTextEdit *editor) : QWidget(&parent) {
    m_editor = editor;
    m_dateTime = true;

	auto & data = Data::getInst(); // anti-pattern singleton

	sysini = data.ToStrIni(data.ini::SYS);
	systemini = data.ToStrIni(data.ini::SYSTEM);
	m_simulini = data.ToStrIni(data.ini::SIMUL);
	m_ipall = data.ToStrNetwork(data.network::IP_ALL);
	m_date = data.ToStrParams(data.params::P_DATE);
	m_ipvis = data.ToStrNetwork(data.network::IP_VIS);
	m_ipvisall = data.ToStrNetwork(data.network::IP_VIS_ALL);
	m_ipdpa = data.ToStrSection(data.section::IP_DPA);
	m_ipdpb = data.ToStrSection(data.section::IP_DPB);
	m_ports = data.ToStrSection(data.section::PORTS);
	m_server = data.ToStrSection(data.section::SERVER);
	m_visual = data.ToStrSection(data.section::SECTION_VISUAL);
	m_visserver = data.ToStrSection(data.section::VISSERVER);

    if (!fileName.isEmpty()) {
		m_file = new QFile;
		m_file->setFileName(fileName);
		m_file->setPermissions(QFileDevice::ReadOwner | QFileDevice::WriteOwner);

		if (m_file->size() >= 2.0E9) { m_file->remove(); }

		m_file->open(QIODevice::Append | QIODevice::Text);
    }
}

void Logger::write(const QString &string, int c) {
    QString text = string;
    if (m_dateTime)
		text = QDateTime::currentDateTime().toString(m_date) + text;
    QTextStream out(m_file);

    #ifdef Q_WS_WIN
    out.setCodec("Windows-1251");
    #else
    out.setCodec("UTF-8");
    #endif

    if (m_file != 0) { out << text + "\r\n"; }

    if(m_editor != 0)
    {
        switch(c){
        case 0 :
            m_editor->setTextColor(Qt::darkGreen);
			break;
        case 1 :
            m_editor->setTextColor(Qt::darkRed);
			break;
        case 2 :
            m_editor->setTextColor(Qt::blue);
			break;
        default :
            m_editor->setTextColor(Qt::black);
			break;
        }
    }

    m_editor->append(text);
}

void Logger::setDateTime(bool value) { m_dateTime = value; }

bool Logger::isSysIniCorrect(){
	QFile sys(sysini);

    bool sysNotExist = !sys.exists();
    bool sysNotOpen = !sys.open(QIODevice::ReadOnly);

    if (sysNotExist || sysNotOpen){
        QString msg = (sysNotExist) ? "exist" : "opened. Check access to file.";
		Logger::write(sysini + " is not " + msg, 1);
        return false;
    } else {
		QSettings sys(sysini, QSettings::IniFormat);

        QStringList sections = sys.childGroups();

        QStringList reqSections;
		reqSections.append(m_ports);
		reqSections.append(m_server);
		reqSections.append(m_visual);

        bool isKeyExist = true;

        foreach(QString reqSection, reqSections){
            bool sysHasNotSection = !sections.contains(reqSection, Qt::CaseInsensitive);
            if  (sysHasNotSection){
				Logger::write(sysini + " has not contain section[" + reqSection + "] or it's empty", 1);
                return false;
            } else {
                sys.beginGroup(reqSection);
				if (!Logger::isSectionContainsKeys(reqSection, sys.allKeys())) { isKeyExist = false; }
                sys.endGroup(); 
            }
        }
        return isKeyExist;
    }
}

bool Logger::isSectionContainsKeys(QString reqSection, QStringList keys){
    if(keys.empty()){
		Logger::write(sysini + " has not contain any key in section[" + reqSection + "]", 2);
        return false;
    }
    bool contains = false;
	if (reqSection == m_ports){
		bool isServerExist = keys.contains(m_server, Qt::CaseInsensitive);
        bool isVisServerExist = keys.contains(m_visserver, Qt::CaseInsensitive);

        if(!isServerExist || !isVisServerExist){
			QString msg = (!isServerExist) ? m_server + " " : m_visserver + " ";
			Logger::write(sysini + " has not contain key " + msg + "in section[" + reqSection + "]", 2);
            contains = false;
        } else {
            contains = true;
        }
	}
	else if (reqSection == m_server){
        bool isIpDPAExist = keys.contains(m_ipdpa, Qt::CaseInsensitive);
        bool isIpDPBExist = keys.contains(m_ipdpb, Qt::CaseInsensitive);

        if(!isIpDPAExist || !isIpDPBExist){
            QString msg = (!isIpDPAExist) ? m_ipdpa + " " : m_ipdpb + " ";
			Logger::write(sysini + " has not contain key " + msg + "in section[" + reqSection + "]", 2);
            contains = false;
        } else {
            contains = true;
        }
    } else if(reqSection == m_visual){
		bool ism_ipallExist = keys.contains(m_ipall, Qt::CaseInsensitive);
        if(!ism_ipallExist){
			Logger::write(sysini + " has not contain key " + m_ipvisall + " in section[" + reqSection + "]", 2);
        }

        for(int i=0; i < keys.size(); i++){
			if (keys.at(i) == m_ipall) continue;

			 bool isIpVisExist = keys.contains(m_ipvis + QString::number(i + 1), Qt::CaseInsensitive);
             if(!isIpVisExist){
				 Logger::write(sysini + " has not contain key " + m_ipvis + QString::number(i) + " in section[" + reqSection + "]", 2);
                 contains = false;
             }
        }
        contains = (ism_ipallExist && contains);
    }
    return contains;
}

bool Logger::ism_simuliniCorrect(std::vector<std::string> & values){
    QDir dir = QDir::current();
         dir.cdUp();

    foreach(std::string value, values){
		QString path = dir.path() + "/" + QString::fromStdString(value) + "/" + m_simulini;

        QFile s(path);

        bool simulNotExist = !s.exists();
        bool simulNotOpen = !s.open(QIODevice::ReadOnly);

        if (simulNotExist || simulNotOpen){
            QString msg = (simulNotExist) ? "exist" : "opened. Check access to file.";
			Logger::write(m_simulini + " is not " + msg + " in path : " + dir.path() + " / " + QString::fromStdString(value) + "\n", 1);
            return false;
        } else {
			Logger::write(m_simulini + " is loaded successfully in path : " + dir.path() + " / " + QString::fromStdString(value) + "\n", 0);
        }
    }

    return true;
}

bool Logger::isSystemIniCorrect(){
    QDir dir = QDir::current();
         dir.cdUp();
		 QString path = dir.path() + "/" + systemini;

    if(QFile::exists(path)) {
		Logger::write(systemini + " is exist", 0);
        return true;
    } else {
		Logger::write(systemini + " is not exist", 1);
        return false;
    }
}

Logger::~Logger() {
	if (m_file != 0) { m_file->close(); }
}
