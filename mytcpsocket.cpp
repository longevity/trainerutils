#include "mytcpsocket.h"

MyTcpSocket::MyTcpSocket(QObject *parent) :
    QObject(parent){ }

void MyTcpSocket::doConnect(QString ip, QString shipName, qint64 port)
{
    socket = new QTcpSocket(this);

    socket->connectToHost(ip, port);

    if(socket->waitForConnected())
    {
        QByteArray bay (shipName.toStdString().c_str());
        socket->write(bay);
        socket->waitForBytesWritten();
        socket->waitForReadyRead();
        socket->close();
    } else { }
}
