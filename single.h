#ifndef SINGLE_H
#define SINGLE_H

#include "Data.h"

#include <QObject>
#include <QString>

class Single : public QObject
{
    Q_OBJECT

public:
	Single::Single() : 
		m_visName(Data::getInst().ToStrWidgets(Data::getInst().widgets::PLATFORM) + "2"),
		m_ipAddress(Data::getInst().ToStrNetwork(Data::getInst().network::ALL_IP)),
		m_shipName(Data::getInst().getInitialShip()) {}

    QString getShipName() { return m_shipName; }
    QString getVisName() { return m_visName; }
       void setVisName(const QString &visName);
    QString getIpAddress(){ return m_ipAddress; }

private:
    QString m_visName;
    QString m_ipAddress;
    QString m_shipName;

public slots:
    void showAboutProgram(void);
    void overwriteSystemINI(void);
    void startVisualPrograms(void);
    void startAllProgramsOfSystem();
    void setShipName();
    void comboboxValueChanged(QString value);
};

#endif
