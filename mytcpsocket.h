#ifndef MYTCPSOCKET_H
#define MYTCPSOCKET_H

#include <QObject>
#include <QTcpSocket>
#include <QString>

class MyTcpSocket : public QObject
{
    Q_OBJECT
public:
    explicit MyTcpSocket(QObject * parent = Q_NULLPTR);

    void doConnect(QString ip, QString shipName, qint64 port);

private:
    QTcpSocket * socket;
};

#endif

