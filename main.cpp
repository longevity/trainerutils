#include "widget.h"
#include "logger.h"
#include "single.h"
#include "simul.h"
#include "Data.h"
#include "mytcpsocket.h"

#include <qobject.h>
#include <QCoreApplication>
#include <QApplication>
#include <QtGui>
#include <QtWidgets>
#include <QSplashScreen>

#include <QDir>
#include <QFile>
#include <QRegExp>
#include <QIODevice>
#include <QStringRef>
#include <QStringList>

#include <QPixmap>
#include <QDateTime>

#include <QSpinBox>
#include <QGroupBox>
#include <QComboBox>
#include <QMessageBox>

#include <QSettings>
#include <QVariant>
#include <QPlainTextEdit>
#include <QSizePolicy>

#include <string>
#include <iostream>

#include <numeric>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QWidget wgt;

	auto & data = Data::getInst(); // anti-pattern singleton

	QSplashScreen* splash = new QSplashScreen(QPixmap(data.ToStrPath(data.IMAGES)+data.ToStrImages(data.NAVIS)));
	splash->showMessage(data.ToStrMessages(data.messages::LOADING_MODULES), Qt::AlignLeft | Qt::AlignBottom, Qt::white);

    splash->show();

    QTimer::singleShot(data.SPLASH_TIME, splash, SLOT(close()));
	QTimer::singleShot(data.SPLASH_TIME, &wgt, SLOT(show()));

    Single singleton;
    Simul simul;

    /* ------- right block with ship's buttons -------*/

    std::map<std::string,
		std::string> ships = data.getShips();

    std::vector<std::string> values;
                             values.reserve(ships.size());

    for (std::map<std::string, std::string>::iterator it = ships.begin();
                                            it != ships.end();
                                          ++it)
    {
        values.push_back(it->second);
    }

    QGridLayout* right = new QGridLayout();
                 right->setSpacing(20);

    std::size_t i =
    std::accumulate(ships.begin()
                   ,ships.end()
                   ,-1
                   ,[&ships, &singleton, &simul, right](int i, const std::pair<std::string,std::string>& ship)->int
    {
        QPointer<QPushButton> vessel = new QPushButton(QString::fromStdString(ship.first));
                     vessel->setObjectName(QString::fromStdString(ship.second));
        QObject::connect(vessel, SIGNAL(clicked()),&singleton, SLOT(setShipName()));
        QObject::connect(vessel, SIGNAL(clicked()),&simul, SLOT(setShipName()));++i;
        QObject::connect(vessel, SIGNAL(clicked()),&singleton, SLOT(overwriteSystemINI()));

        right->addWidget(vessel, i, 1);

        vessel.clear();

        return i;
    });     if(i != ships.size() - 1) { /* memory leak in cycle */ } else { std::map<std::string, std::string>().swap(ships); }

	QPushButton* execute = new QPushButton(data.ToStrWidgets(data.widgets::EXECUTE));
    QObject::connect(execute, SIGNAL(clicked()),&singleton, SLOT(startAllProgramsOfSystem()));

	QPushButton* quit = new QPushButton(data.ToStrWidgets(data.widgets::QUIT));
    QObject::connect(quit, SIGNAL(clicked()),&app, SLOT(quit()));

    right->addWidget(quit,  11, 1);

    /* -------- left block with platform's buttons -------*/

	QGroupBox* groupSim = new QGroupBox(data.ToStrWidgets(data.widgets::SETTINGS_SIMUL));
               groupSim->setAlignment(Qt::AlignCenter);
               groupSim->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
			   QGroupBox* groupVis = new QGroupBox(data.ToStrWidgets(data.widgets::SETTINGS_VISUAL));
               groupVis->setAlignment(Qt::AlignCenter);
               groupVis->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);

    QVBoxLayout* vBoxSim = new QVBoxLayout;

    std::vector<std::string> platforms;
	platforms.push_back((data.ToStrWidgets(data.widgets::PLATFORM) + "1").toStdString());
	platforms.push_back((data.ToStrWidgets(data.widgets::PLATFORM) + "2").toStdString());
	platforms.push_back((data.ToStrWidgets(data.widgets::PLATFORM) + "3").toStdString());

    std::for_each(platforms.begin(), platforms.end(), [&simul, vBoxSim](std::string const& platform)
    {
        QPointer<QPushButton> platf = new QPushButton(QString::fromStdString(platform));
                              platf->setObjectName(QString::fromStdString(platform));
        (platf->objectName().contains("1")) ? platf->setEnabled(false) : platf->setEnabled(true);
        QObject::connect(platf, SIGNAL(clicked()),&simul, SLOT(overwritem_simulini()));

        vBoxSim->addWidget(platf); /* memory leak in cycle */

        platf.clear();
    });

    std::vector<std::string>().swap(platforms);

    /* ------- center block with visual's buttons ------*/

    QVBoxLayout* vBoxVis = new QVBoxLayout;

    std::vector<std::string> visuals;
	visuals.push_back((data.ToStrWidgets(data.widgets::VISUAL) + "1").toStdString());
	visuals.push_back((data.ToStrWidgets(data.widgets::VISUAL) + "2").toStdString());
	visuals.push_back((data.ToStrWidgets(data.widgets::VISUAL) + "3").toStdString());

    std::for_each(visuals.begin(), visuals.end(), [&singleton, vBoxVis](std::string const& visual)
    {
        QPointer<QPushButton> vis = new QPushButton(QString::fromStdString(visual));
                              vis->setObjectName(QString::fromStdString(visual).replace(
								  "Visual", "Platform"));
        (vis->objectName().contains("1")) ? vis->setEnabled(false) : vis->setEnabled(true);
        QObject::connect(vis, SIGNAL(clicked()),&singleton, SLOT(startVisualPrograms()));

        vBoxVis->addWidget(vis); /* memory leak in cycle */

        vis.clear();
    });

    std::vector<std::string>().swap(visuals);

    QSpinBox* spinSim = new QSpinBox;
              spinSim->setRange(1, 2);

    QObject::connect(spinSim, SIGNAL(valueChanged(int)), &simul, SLOT(spinboxValueChanged(int)));

    vBoxSim->addWidget(spinSim);
    vBoxSim->addStretch(1);
    groupSim->setLayout(vBoxSim);

	QSettings sys(data.ToStrIni(data.ini::SYS), QSettings::IniFormat);
			  sys.beginGroup(data.ToStrWidgets(data.widgets::VISUAL));

    QStringList ipList;

    foreach(QString key, sys.allKeys()){
		if (!key.contains(data.ToStrNetwork(data.network::IP_VIS), Qt::CaseInsensitive)) continue;
        QString ip = sys.value(key, "").toString();
        ipList.append(ip);
    }

    QComboBox* comboVisual = new QComboBox();
    comboVisual->addItems(ipList);
    for(int i=0; i<comboVisual->count(); i++)
        comboVisual->setItemData(i, Qt::AlignRight, Qt::TextAlignmentRole);
    QObject::connect(comboVisual, SIGNAL(currentTextChanged(QString)), &singleton,
                        SLOT(comboboxValueChanged(QString)));
    comboVisual->setCurrentIndex(comboVisual->count() - 1);

    vBoxVis->addWidget(comboVisual);
    vBoxVis->addStretch(1);
    groupVis->setLayout(vBoxVis);

    /* -------- left block with platform's buttons ------- */

    QHBoxLayout* hbl = new QHBoxLayout;
                 hbl->setSpacing(25);

				 QPushButton* about = new QPushButton(data.ToStrWidgets(data.widgets::ABOUT));
				 about->setObjectName(data.ToStrWidgets(data.widgets::ABOUT));
    QObject::connect(about, SIGNAL(clicked()),&singleton, SLOT(showAboutProgram()));

    QTextEdit *editor = new QTextEdit();
               editor->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
               editor->setReadOnly(true);
			   editor->setStyleSheet(data.ToStrParams(data.params::P_RGB));

    QGridLayout *left = new QGridLayout;
                 left->addWidget(groupSim, 0, 0);
                 left->addWidget(groupVis, 0, 1);
                 left->addWidget(editor, 1, 0, 1, 2);
                 left->addWidget(execute, 2, 0);
                 left->addWidget(about, 2, 1);

    Logger* log = new Logger(wgt, data.ToStrIni(data.LOG), editor);
			log->write(data.ToStrMessages(data.messages::APP_INIT), 0);
			log->write(data.ToStrPath(data.IMAGES), 1);
			log->write(data.ToStrImages(data.NAVIS), 2);
            log->isSysIniCorrect();
            log->isSystemIniCorrect();
            log->ism_simuliniCorrect(values);

    std::vector<std::string>().swap(values);

    QImage logo;

    if(logo.load(data.ToStrPath(data.IMAGES)+data.ToStrImages(data.LOGO))){
		log->write("Loading " + data.ToStrImages(data.images::LOGO) + " is success...", 0);
    } else {
		log->write("Loading " + data.ToStrImages(data.images::LOGO) + " is failed...", 1);
    }

    QImage img = logo.scaled(150, 485, Qt::KeepAspectRatio);

    QLabel *image = new QLabel;
            image->setPixmap(QPixmap::fromImage(img));
            image->setScaledContents(true);

    QGridLayout* center = new QGridLayout;
                 center->addWidget(image);

    hbl->addLayout(left, 0);
    hbl->addLayout(center,0);
    hbl->addLayout(right, 0);

	wgt.setWindowIcon(QIcon(data.ToStrIcons(data.icons::SHIP_ICO)));
	wgt.setWindowTitle(data.ToStrWidgets(data.widgets::TRAINER_UTIL));
    wgt.setLayout(hbl);

    wgt.resize(300, 500);
    wgt.adjustSize();
    wgt.move(QApplication::desktop()->screen()->rect().center() - wgt.rect().center());

    return app.exec();
}
