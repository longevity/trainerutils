#ifndef LOGGER_H
#define LOGGER_H

#include <QObject>
#include <QPlainTextEdit>
#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include <QStringList>

#include "simul.h"

class Logger : public QWidget
{
    Q_OBJECT
public:
    explicit Logger(QWidget &parent, QString fileName, QTextEdit *editor = Q_NULLPTR);
    ~Logger();
    void setDateTime(bool value);
    bool isSysIniCorrect();
    bool ism_simuliniCorrect(std::vector<std::string> & values);
    bool isSectionContainsKeys(QString reqSection, QStringList keys);
    bool isSystemIniCorrect();

private:
    QFile * m_file;
    QTextEdit *m_editor;

	QString sysini;
	QString systemini;
	QString m_simulini;
	QString m_ipall;
	QString m_ipvis;
	QString m_ipvisall;
	QString m_ipdpa;
	QString m_ipdpb;
	QString m_date;
	QString m_ports;
	QString m_server;
	QString m_visual;
	QString m_visserver;

    bool m_dateTime;

signals:

public slots:
    void write(const QString & string, int c);
};

#endif
